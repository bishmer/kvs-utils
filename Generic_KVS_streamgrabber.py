import boto3
import cv2

#DEFINE THESE
STREAM_NAME = ''
AWS_REGION=''
AWS_KEY=''
AWS_SECRET=''

kvs = boto3.client("kinesisvideo",
                    region_name=AWS_REGION,
                    aws_access_key_id=AWS_KEY,
                    aws_secret_access_key=AWS_SECRET)

# Grab the endpoint from GetDataEndpoint
endpoint = kvs.get_data_endpoint(
    APIName="GET_HLS_STREAMING_SESSION_URL",
    StreamName=STREAM_NAME
)['DataEndpoint']

#print(endpoint)

kvam = boto3.client("kinesis-video-archived-media", endpoint_url=endpoint, region_name=AWS_REGION, aws_access_key_id=AWS_KEY,
                    aws_secret_access_key=AWS_SECRET)
url = kvam.get_hls_streaming_session_url(
    StreamName=STREAM_NAME,
    PlaybackMode="LIVE"
)['HLSStreamingSessionURL']

vcap = cv2.VideoCapture(url)

while(True):
    # Capture frame-by-frame
    ret, frame = vcap.read()

    if frame is not None:
        # Display the resulting frame
        cv2.imshow('frame',frame)

        # Press q to close the video windows before it ends if you want
        if cv2.waitKey(22) & 0xFF == ord('q'):
            break
    else:
        print("Frame is None")
        break

# When everything done, release the capture
vcap.release()
cv2.destroyAllWindows()
